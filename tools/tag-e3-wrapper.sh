#!/bin/bash

# Description:
# Script to create tag for E3 wrapper module
# Author: Wayne Lewis
# Date: 2020-07-13

# Options:
# Process options

while getopts "tpvh" opt; do
  case $opt in
    t) tag_repo=true
      ;;
    p) push=true
      ;;
    v) verbose=true
      ;;
    h) help=true
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Create tag for E3 wrapper module repository"
  echo "usage: ./tag-e3-wrapper [-v] [-h]"
  echo "parameters:"
  echo "-t: tag repository"
  echo "-p: push to remote repository"
  echo "-v: be verbose"
  echo "-h: print this usage information"
  exit 0
fi

generate_tag() {
  # Create the tag components
  local base_version=`grep -e '^EPICS_BASE' configure/RELEASE | awk -F- '{print $NF}'`
  local require_version=`grep -e '^E3_REQUIRE_VERSION' configure/RELEASE | cut -f 2 -d '='`
  local module_version=`grep -e '^E3_MODULE_VERSION' configure/CONFIG_MODULE | cut -f 2 -d '='`;
  local git_commit=`git rev-parse --verify --short HEAD`
  local datestamp=`date +%Y%m%d`
  local timestamp=`date +%H%M%S`
  # Assemble the complete tag
  tag="$base_version-$require_version/$module_version-$git_commit-$datestamp"T"$timestamp"

  if [ "$verbose" = true ]; then
    echo "base_version = $base_version"
    echo "require_version = $require_version"
    echo "module_version = $module_version"
    echo "git_commit = $git_commit"
    echo "datestamp = $datestamp"
    echo "timestamp = $timestamp"
  fi
}

get_remote() {
  remote="ess-ssh"
}

tag_wrapper_module() {
  # Check that we are in the right part of the directory tree.
  if [[ $PWD = */e3-* ]]; then

    [ "$verbose" = true ] && echo "$PWD"
    generate_tag

    [ "$verbose" = true ] && echo "Generated tag = $tag"
   
    # Tag the repository
    if [ "$tag_repo" = true ]; then 
      git tag $tag
      [ "$verbose" = true ] && echo "Tagged repo with $tag"
    fi

    # Push to the remote
    if [ "$push" = true ]; then
      get_remote
      [ "$verbose" = true ] && echo "Remote = $remote"
      # Only push if we get a valid response for the remote name
      if [ ! -z "$remote" ]; then 
        git push $remote $tag
        [ "$verbose" = true ] && echo "Pushed $tag to $remote"
      fi
    fi
  else
    echo "Not in an E3 module wrapper directory"
    exit -1
  fi
}

tag_wrapper_module
