#!/bin/bash

# Description:
# Script to clone a group of modules
# Author: Wayne Lewis
# Date: 2020-07-15

# Options:
# -f: input inventory file
# -g: group to clone
# -v: verbose
# -s: use https instead of ssh to clone
# -h: help

help() {
  echo "Clone all modules whose path matches a defined gitlab subgroup name."
  echo ""
  echo "usage: ./clone-group-modules.sh -f <path/to/inventory/file> -g <group_name> [-v] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-g: specify group to clone"
  echo "-s: use https instead of ssh to clone"
  echo "-v: be verbose"
  echo "-h: print this usage information"
}

# Process options
while getopts "f:g:b:r:vsh" opt; do
  case "$opt" in
    f) input_file="$OPTARG"
      ;;
    g) group="$OPTARG"
      ;;	
    b) base_version="$OPTARG"
      ;;
    r) require_version="$OPTARG"
      ;;
    v) verbose=true
      ;;
    s) ssh=false
      ;;
    h) help
      exit 0
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      help
      exit 1
      ;;
  esac
done

clone_modules() {
  pwd=`pwd`
  [ "$verbose" = true ] && echo "Current path: $pwd"

  pushd "$group" > /dev/null
  # Iterate through the inventory file. # lines are ignored as comments.
  for dir in $1; do
    module=$(basename "$dir")
    [ "$verbose" = true ] && echo "  module: $dir"
    if [ "$ssh" = false ]; then
      git clone --origin ess-https https://gitlab.esss.lu.se/e3/"$dir".git
    else
      git clone --origin ess-ssh git@gitlab.esss.lu.se:e3/"$dir".git
    fi
    pushd $module > /dev/null
    tags=$( git for-each-ref --format '%(refname:short)' --sort=creatordate refs/tags )
    [ "$verbose" = true ] && echo "  all tags:" && echo "$tags" | sed 's/^/    /'
    if [ -n "$base_version" ]; then
      [ "$verbose" = true ] && echo "  Using base version: $base_version"
      tags=$( echo "$tags" | grep "$base_version" )
      if [ -n "$require_version" ]; then
        [ "$verbose" = true ] && echo "  Using require version: $require_version"
        tags=$( echo "$tags" | grep "$require_version" )
      fi
      tag=$( echo "$tags" | tail -n 1 )
      if [ -n "$tag" ]; then
        [ "$verbose" = true ] && echo "  Checking out tag: $tag"
        git checkout "$tag"
      else
        echo "  No tag for EPICS base $base_version and require $require_version exists." >&2 
      fi
    fi
    popd
  done
  popd
}

[ ! -f "$input_file" ] && echo "Input file '$input_file' does not exist." >&2 && exit 1

[ -z "$group" ] && echo "You must specify a group" >&2 && exit 1

# Create the group directory
[ -d "$group" ] || mkdir -p "$group"

# We ignore any comments that start with #, such as a line like this.
modules=$( cat "$input_file" | sed 's/#.*//' | grep e3 | grep "^$group" )

if [ -z "$modules" ]; then
  echo "Error: No modules found for group $group in inventory file $input_file" >&2
  exit 1
fi

[ "$verbose" = true ] && echo "Modules to clone:" && echo "$modules" | sed 's/^/  /'

clone_modules "$modules"
