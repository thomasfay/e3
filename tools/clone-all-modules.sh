#!/bin/bash

# Description:
# Script to clone all modules in inventory
# Author: Wayne Lewis
# Date: 2020-08-03

# Options:
# -f: input inventory file
# -b: EPICS base version
# -r: require version
# -l: latest tagged version
# -v: verbose
# -h: help

help() {
  echo "Clone all modules from inventory file. Place in subdirectories that match gitlab subgroup name."
  echo "Optionally, check out the latest tagged version that matches the provided EPICS base and require versions."
  echo "By default, check out the latest version on the default branch."
  echo ""
  echo "usage: ./clone-all-modules.sh -f <path/to/inventory/file> [-b <epics_base_version>] [-r <require_version>] [-v] [-s] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-b: EPICS base version"
  echo "-r: require version"
  echo "-v: be verbose"
  echo "-s: use https instead of ssh to clone"
  echo "-h: print this usage information"
}

# Process options
while getopts "f:b:r:vsh" opt; do
  case $opt in
    f) input_file="$OPTARG"
      ;;
    b) base_version="$OPTARG"
      ;;
    r) require_version="$OPTARG"
      ;;
    v) verbose=true
      ;;
    s) ssh=false
      ;;
    h) help
      exit 0
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
      help
      exit 1
      ;;
  esac
done


clone_modules() {
  script=$(realpath $0)
  script_path=$(dirname "$script")

  [ "$verbose" = true ] && verbose_flag=-v
  [ "$ssh" = false ] && ssh_flag=-s
  [ -n "$base_version" ] && base_arg="-b $base_version"
  [ -n "$require_version" ] && require_arg="-r $require_version"

  group_names=(common ts ecat ifc ps rf devices vac area psi bi)
  for group_name in ${group_names[*]}; do
    [ "$verbose" = true ] && echo "Cloning group $group_name"
    bash -c "$script_path/clone-group-modules.sh -f $input_file -g $group_name $ssh_flag $base_arg $require_arg"
  done
}

[ ! -f "$input_file" ] && echo "Input file '$input_file' does not exist." >&2 && exit 1

clone_modules
